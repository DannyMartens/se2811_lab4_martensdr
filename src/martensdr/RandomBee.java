package martensdr;

/*
 * SE 2811
 * Winter 2017/18
 * Lab 3
 * Names: Brandon Kandarapally, Danny Martens
 * Created: 12/28/2017
 */

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;

/**
 * @author Brandon Kandarapally, Danny Martens
 * @version 1.0
 * @created 14-Dec-2017 3:52:09 PM
 * Bee that will randomly choose a flower in the garden and move to it.
 */
public class RandomBee extends Bee {

    /**
     * Variables unique to Random Bee
     */
    private int currentFlowerNum;

    /**
     * Default Constructor
     */
	public RandomBee(){
	    super();
	    currentFlowerNum = -1;
		view = new ImageView(new Image("File:randomBee.png"));
		view.setX(xCord);
		view.setY(yCord);
    }

	public void finalize() throws Throwable {
		super.finalize();
	}

    /**
     * Calculates distance from a flower
     * @param flowerX X coordinate of flower
     * @param flowerY Y coordinate of flower
     * @return double value of distance from a flower
     */
	public double calculateDistance(double flowerX, double flowerY){
		return super.calculateDistance(flowerX,flowerY);
	}

    /**
     * Determines if Bee has reached it's target flower, choose a new flower if needed
     * @param flowers flowers in garden
     * @param bees bees in garden
     */
	public void checkForFlower(ArrayList<Flower> flowers,ArrayList<Bee> bees){
		if (!isDead()) {
			if (currentFlowerNum == -1) {
				currentFlowerNum = (int) (Math.random() * flowers.size());
			}
			Flower flower = flowers.get(currentFlowerNum);
			double distance = calculateDistance(flower.getxCord(), flower.getyCord());
			if (distance < 3) {
				goToFlower(flowers.get(currentFlowerNum));
				currentFlowerNum = (int) (Math.random() * flowers.size());
			} else {
				moveTick(flowers.get(currentFlowerNum));
			}

		}
		else{
		    energy = 0;
		    view.setImage(new Image("File:dead.png"));
          //  view = new ImageView(new Image("File:dead.png"));
			//System.out.println("Bee is dead");
		}
	}

    /**
     * Goes to flower once it is reached and gets nectar
     * @param flower a flower in the garden
     */
	public void goToFlower(Flower flower){
		super.goToFlower(flower);
	}

    /**
     * Uses difference between bee's current coordinates and flower's coordinate to determine how to move to get closer;
     * @param flower Target Flower
     */
	public void moveTick(Flower flower){
		double xDif = xCord - flower.getxCord();
		double yDif = yCord - flower.getyCord();

		if (xDif < 0){
			xCord = xCord + 3;
		}
		else if ( xDif > 0){
			xCord = xCord -3;
		}

		if (yDif < 0){
			yCord = yCord +3;
		}
		if (yDif > 0){
			yCord = yCord - 3;
		}

		energy = energy -1;
	}

    /**
     * Deterimes if bee is dead
     * @return true if energy is less than or equal to 0
     */
	public boolean isDead(){
		return energy<=0;
	}



}