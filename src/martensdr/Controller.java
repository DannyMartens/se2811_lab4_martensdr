package martensdr;

/*
 * SE 2811
 * Winter 2017/18
 * Lab 3
 * Names: Danny Martens, Brandon Kandarapally
 * Created: 12/28/2017
 */

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;


import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private Pane gardenPane;

    private ArrayList<Bee> bees;
    private ArrayList<Flower> flowers;
    private ArrayList<Label> beeLabels;
    private ArrayList<Label> plantLabels;
    private MediaPlayer player;
    private Label energyLabel;
    private Label plantLabel;
    private ImageView key;

    /**
     * Runs the pane, then has the logic for when the user clicks right on the keyboard.
     * When this happens a call to the Bee class happens for moving the bees to the right and checking
     * to see if they are close enough to a flower.
     */

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        key = new ImageView();
        energyLabel = new Label("");
        plantLabel = new Label("");
        bees = new ArrayList<>();
        flowers = new ArrayList<>();
        beeLabels = new ArrayList<>();
        plantLabels = new ArrayList<>();
        playMusic();
        gardenPane.getChildren().add(new ImageView(new Image("File:grass.png")));
        gardenPane.getChildren().add(new ImageView(new Image("File:instructions.png")));
        createGarden();
        gardenPane.getChildren().add(energyLabel);
        gardenPane.setOnKeyPressed(ke -> {
            if (ke.getCode().equals(KeyCode.RIGHT)) {
                for(int i = 0; i < bees.size(); i++) {
                    Bee tempBee = bees.get(i);
                    bees.get(i).checkForFlower(flowers,bees);
                    bees.get(i).getView().setY(tempBee.yCord);
                    bees.get(i).getView().setX(tempBee.xCord);



                    int amountOfEnergy = bees.get(i).energy;
                    String energy =  Integer.toString(amountOfEnergy);
                    //used to updated the labels
                    for(int j = 0; j < beeLabels.size(); j++) {
                        beeLabels.get(i).setText(energy);
                        beeLabels.get(i).relocate(bees.get(i).xCord-10,bees.get(i).yCord-10);
                    }
                    for(int k = 0; k < plantLabels.size(); k++) {
                        plantLabels.get(i).setText(Integer.toString(flowers.get(i).nectar));
                    }

                    /*
                    BUG: only works for random bees straight bee goes right over flowers.
                    Left to do: - Make a key
                     */

                }


               //System.out.println(bees.get(0).energy + "|" + bees.get(1).energy + "|" + bees.get(2).energy + "|" + bees.get(3).energy);
            }
            if (gameOver()){
                player.stop();
                gardenPane.getChildren().add(new ImageView(new Image("File:gameOver.png")));
               playGameOver();
            }
            if (ke.getCode().equals(KeyCode.SPACE) && gameOver()){
                bees.clear();
                flowers.clear();
                gardenPane.getChildren().clear();
                player.stop();
                initialize(location, resources);
            }
        });
    }
    /**
     * creates the garden to populate with all the bees and flowers
     */
    private void createGarden(){
        for (int i = 0; i<5; i++){
            Flower flower = new HealthPlant();
            Label label = new Label(Integer.toString(flower.nectar));
            label.setTextFill(Color.rgb(255,255,255));
            flowers.add(flower);
            gardenPane.getChildren().add(flower.getView());
            label.setMinWidth(50);
            label.setMinHeight(50);
            label.setText(Integer.toString(flower.nectar));
            label.relocate(flower.xCord-10,flower.yCord-10);
            gardenPane.getChildren().add(label);
            plantLabels.add(label);
        }
        for (int i = 0; i<2; i++){
            Flower flower = new DeathPlant();
            Label newLabel = new Label(Integer.toString(flower.nectar));
            newLabel.setTextFill(Color.rgb(255,255,255));
            flowers.add(flower);
            gardenPane.getChildren().add(flower.getView());
            newLabel.setMinWidth(50);
            newLabel.setMinHeight(50);
            newLabel.setText(Integer.toString(flower.nectar));
            newLabel.relocate(flower.xCord-10,flower.yCord-10);
            gardenPane.getChildren().add(newLabel);
            plantLabels.add(newLabel);
        }
        //two fast bee's.
        for (int i = 0; i<2; i++){
            Bee bee = new StraightBee();
            bee = new Speed(bee);
            Label newLabel = new Label(Integer.toString(bee.energy));
            newLabel.setTextFill(Color.rgb(255,255,255));
            bees.add(bee);
            gardenPane.getChildren().add(bee.getView());
            newLabel.setMinWidth(50);
            newLabel.setMinHeight(50);
            newLabel.setText(Integer.toString(12341234));
            newLabel.relocate(bee.xCord-10,bee.yCord-10);
            gardenPane.getChildren().add(newLabel);
            beeLabels.add(newLabel);
        }
        //two shield bee's.
        for (int i = 0; i<2; i++){
            Bee bee = new StraightBee();
            bee = new Shield(bee);
            Label newLabel = new Label(Integer.toString(bee.energy));
            newLabel.setTextFill(Color.rgb(255,255,255));
            bees.add(bee);
            gardenPane.getChildren().add(bee.getView());
            newLabel.setMinWidth(50);
            newLabel.setMinHeight(50);
            newLabel.setText(Integer.toString(12341234));
            newLabel.relocate(bee.xCord-10,bee.yCord-10);
            gardenPane.getChildren().add(newLabel);
            beeLabels.add(newLabel);
        }
        //two deathwishBees
        for (int i = 0; i<2; i++){
            Bee bee = new StraightBee();
            bee = new Deathwish(bee);
            Label newLabel = new Label(Integer.toString(bee.energy));
            newLabel.setTextFill(Color.rgb(255,255,255));
            bees.add(bee);
            gardenPane.getChildren().add(bee.getView());
            newLabel.setMinWidth(50);
            newLabel.setMinHeight(50);
            newLabel.setText(Integer.toString(12341234));
            newLabel.relocate(bee.xCord-10,bee.yCord-10);
            gardenPane.getChildren().add(newLabel);
            beeLabels.add(newLabel);

        }
        // two normal bee's
        for (int i = 0; i<2; i++){
            Bee bee = new StraightBee();
            Label newLabel = new Label(Integer.toString(bee.energy));
            newLabel.setTextFill(Color.rgb(255,255,255));
            bees.add(bee);
            gardenPane.getChildren().add(bee.getView());
            newLabel.setMinWidth(50);
            newLabel.setMinHeight(50);
            newLabel.setText(Integer.toString(bee.energy));
            newLabel.relocate(bee.xCord-10,bee.yCord-10);
            gardenPane.getChildren().add(newLabel);
            beeLabels.add(newLabel);
        }
        for (int i = 0; i<2; i++){
            Bee bee =  new RandomBee();
            Label newLabel = new Label(Integer.toString(bee.energy));
            newLabel.setTextFill(Color.rgb(255,255,255));
            bees.add(bee);
            gardenPane.getChildren().add(bee.getView());
            newLabel.setMinWidth(50);
            newLabel.setMinHeight(50);
            newLabel.setText(Integer.toString(bee.energy));
            newLabel.relocate(bee.xCord-10,bee.yCord-10);
            gardenPane.getChildren().add(newLabel);
            beeLabels.add(newLabel);
        }
    }
    /**
     * Plays awesome background music
     */
    private void playMusic(){
        String uriString = new File("music.mp3").toURI().toString();
        player = new MediaPlayer( new Media(uriString));
        player.setCycleCount(999);
        player.setVolume(1);
        player.play();
    }
    /**
     * Plays sad death music
     */
    private void playGameOver(){
        String uriString = new File("gameOver.mp3").toURI().toString();
        player = new MediaPlayer( new Media(uriString));
        player.setCycleCount(1);
        player.setVolume(1);
        player.play();
    }
    /**
     * sets the pane as the focus so that the event Capture works.
     */
    public void setFocus(){
        gardenPane.requestFocus(); //Has to be called otherwise the wrong thing is focused and  keyEvent will not work
    }
    /**
     * checks to see if all the bees are dead
     * @return returns the status if the bees are all dead.
     */
    private boolean gameOver(){
        for (int i = 0; i<bees.size(); i++){
            if (!bees.get(i).isDead()){
                return false;
            }
        }
        return true;
    }

}
