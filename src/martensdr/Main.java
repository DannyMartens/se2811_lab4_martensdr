package martensdr;
/*
 * SE 2811
 * Winter 2017/18
 * Lab 3
 * Names: Danny Martens, Brandon Kandarapally
 * Created: 12/28/2017
 */
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;

public class Main extends Application {
    static Controller testController;


    /**
     * opens and loads the JavaFX
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("GardenUI.fxml"));
        Parent root = loader.load();
        //Parent root = FXMLLoader.load(getClass().getResource("GardenUI.fxml"));
        primaryStage.setTitle("The Bees And The Flowers: A Garden Tale");
        primaryStage.setScene(new Scene(root, 620, 850));
        primaryStage.setResizable(false);
        File file = new File("icon.png");
        Image image = new Image(file.toURI().toString());
        primaryStage.getIcons().add(image);
        primaryStage.show();
        testController = loader.getController();
        testController.setFocus();
    }
    /**
     * runs the program
     */
    public static void main(String[] args) {
        launch(args);
    }
}
