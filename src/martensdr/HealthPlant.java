package martensdr;
/*
 * SE 2811
 * Winter 2017/18
 * Lab 3
 * Names: Danny Martens, Brandon Kandarapally
 * Created: 12/28/2017
 */

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author Danny Martens, Brandon Kandarapally
 * @version 1.0
 * @created 14-Dec-2017 3:52:09 PM
 * Flower that gives first bee to come to it energy
 */



public class HealthPlant extends Flower {
	/**
	 * creates a good plant with 300 nectar to give.
	 */
	public HealthPlant(){
		super();
		nectar = 300;
		view = new ImageView(new Image("File:healthPlant.png"));
		view.setX(xCord);
		view.setY(yCord);

	}

	public void finalize() throws Throwable {
		super.finalize();
	}
	/**
	 * gives the nectar to the bee and sets the amount to 0.
	 */
	public int giveNectar(){
		int temp = nectar;
		nectar = 0;
		return temp;

	}



}