package martensdr;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;

public class Speed extends BeeDecorator {
    private ImageView view = new ImageView(new Image("File:fastBee.png"));

    public Speed(Bee fastBee) {
        bee = fastBee;
    }
    public void checkForFlower(ArrayList<Flower> flowers, ArrayList<Bee> bees){
        super.checkForFlower(flowers,bees);
    }
    /**
     * calls the abstract method of visiting a flower
     * @param flower the flower to go to.
     */
    public void goToFlower(Flower flower) {
        super.goToFlower(flower);

    }
    /**
     * moves the bee left or right depending on the lap status
     */
    public void moveTick() {
        if (yCord >= 600 || yCord <= 0){
            yLap++;
            if(xLap % 2 == 0) {
                xCord += 30;
            }
            else   {
                xCord -= 30;
            }
        }
        if (yLap % 2 == 0) {
            yCord += 30;
            // energy = energy - 1;
        }
        else {
            yCord -= 30;
            //energy = energy -1;
        }
        if(xCord > 600 || xCord < 0) {
            xLap++;
        }
        energy = energy -1;
    }

    public ImageView getView() {
        return view;
    }

}
