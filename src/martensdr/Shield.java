package martensdr;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;

public class Shield extends BeeDecorator {
    private ImageView view = new ImageView(new Image("File:shieldBee.png"));
    boolean hasShield = true;

    public Shield(Bee shieldBee) {
        bee = shieldBee;
    }
    public void checkForFlower(ArrayList<Flower> flowers, ArrayList<Bee> bees) {

            if (!isDead()) {

                for (int i = 0; i < flowers.size(); i++) {
                    Flower flower = flowers.get(i);
                    double distance = calculateDistance(flower.getxCord(), flower.getyCord());
                    if (distance <= 25) {
                        Flower tempFlower = flowers.get(i);
                        if (hasShield) {
                            if (tempFlower.giveNectar() < 0) {
                                int tempNectar = bee.energy;
                                goToFlower(flowers.get(i));
                                bee.energy = tempNectar;
                                hasShield = false;
                            }
                        }

                    }
                }

                for (int p = 0; p < bees.size() - 1; p++) {
                    double collision = bees.get(p).collision(bees.get(p + 1).xCord, bees.get(p + 1).yCord);
                    if (collision < 25) {
                        if (xCord > 100) {
                            xCord -= 100;
                        } else {
                            xCord += 100;
                        }
                    }
                }
                moveTick();
            } else {
                energy = 0;
                view.setImage(new Image("File:dead.png"));
            }
        }

    /**
     * calls the abstract method of visiting a flower
     * @param flower the flower to go to.
     */
    public void goToFlower(Flower flower) {
        super.goToFlower(flower);


    }
    /**
     * moves the bee left or right depending on the lap status
     *
     */


    public ImageView getView() {
        return view;
    }
}
