package martensdr;
/*
 * SE 2811
 * Winter 2017/18
 * Lab 3
 * Names: Danny Martens, Brandon Kandarapally
 * Created: 12/28/2017
 */

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.util.ArrayList;

/**
 * @author Danny Martens, Brandon Kandarapally
 * @version 1.0
 * @created 14-Dec-2017 3:52:09 PM
 * Bee that just moves straight with no target flower
 */

public class StraightBee extends Bee {

	private int yLap;
	private int xLap;

    /**
     * Straight bee where the laps signify if it should go left or right
     */
	public StraightBee(){
		super();
		yLap = 1;
		xLap = 1;
		view = new ImageView(new Image("File:straightBee.png"));
		view.setX(xCord);
		view.setY(yCord);
	}

	public void finalize() throws Throwable {
		super.finalize();
	}
    /**
     * Calculates the distance to a flower
     * @param flowerX xCord for the flower
     * @param flowerY yCord for the flower
     */
	public double calculateDistance(double flowerX, double flowerY){
		return super.calculateDistance(flowerX,flowerY);
	}
    /**
     * Calculates the distance to another bee
     * @param beeX xCord for the bee
     * @param beeY yCord for the bee
     */
	public double collision(double beeX, double beeY) {
        return super.collision(beeX,beeY);
    }
    /**
     * As long as the bee is alive it will check to see if there is a flower close to it.
     * After checking it moves the bee.
     * @param flowers xCord for the bee
     * @param bees yCord for the bee
     */
	public void checkForFlower(ArrayList<Flower> flowers,ArrayList<Bee> bees){

		if (!isDead()) {
			for (int i = 0; i < flowers.size(); i++) {
				Flower flower = flowers.get(i);
				double distance = calculateDistance(flower.getxCord(), flower.getyCord());
				if (distance <= 25) {
					goToFlower(flowers.get(i));
				}

			}

            for (int p = 0; p < bees.size()-1; p++) {
                double collision = bees.get(p).collision(bees.get(p+1).xCord, bees.get(p+1).yCord);
                if (collision < 25) {
                    if(xCord > 100) {
                        xCord -=100;
                    } else {
                        xCord += 100;
                    }
                }
            }
			moveTick();
		}
		else {
		    energy = 0;
			view.setImage(new Image("File:dead.png"));
		}
	}
    /**
     * calls the abstract method of visiting a flower
     * @param flower the flower to go to.
     */
	public void goToFlower(Flower flower) {
		super.goToFlower(flower);

	}
    /**
     * moves the bee left or right depending on the lap status
     */
	public void moveTick() {
	    if (yCord >= 600 || yCord <= 0){
	        yLap++;
	        if(xLap % 2 == 0) {
	            xCord += 10;
            }
            else   {
	            xCord -= 10;
            }
        }
	    if (yLap % 2 == 0) {
            yCord += 10;
           // energy = energy - 1;
        }
        else {
	        yCord -= 10;
			//energy = energy -1;
        }
        if(xCord > 600 || xCord < 0) {
	        xLap++;
        }
        energy = energy -1;
	}


    @Override
    public ImageView getView() {
        return super.getView();
    }
}