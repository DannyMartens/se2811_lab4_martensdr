package martensdr;
/*
 * SE 2811
 * Winter 2017/18
 * Lab 3
 * Names: Danny Martens, Brandon Kandarapally
 * Created: 12/28/2017
 */
import javafx.scene.image.ImageView;

/**
 * @author Danny Martens, Brandon Kandarapally
 * @version 1.0
 * @created 14-Dec-2017 3:52:09 PM
 * Abstract class for a Flower
 */

public abstract class Flower {

	protected int nectar;
	protected double xCord;
	protected double yCord;
	protected ImageView view;

	/**
	 * creates a flower with random cords
	 */

	public Flower(){
		xCord = Math.random() * 600;
		yCord = Math.random() * 600;
	}

	public void finalize() throws Throwable {

	}
    /**
     * gives the bee the nectar and then sets the current nectar to 0.
     */
	public int giveNectar(){
		int temp = nectar;
		nectar = 0;
		return temp;
	}

	public double getxCord() {
		return xCord;
	}
	public double getyCord() {
		return yCord;
	}

    public ImageView getView() {
        return view;
    }
}