package martensdr;

/*
 * SE 2811
 * Winter 2017/18
 * Lab 3
 * Names: Brandon Kandarapally, Danny Martens
 * Created: 12/28/2017
 */

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author Brandon Kandarapally, Danny Martens
 * @version 1.0
 * @created 14-Dec-2017 3:52:09 PM
 * Death Plant version of flower that removes energy from bee rather than giving energy
 */
public class DeathPlant extends Flower {

    /**
     * Default Constructor
     */
	public DeathPlant(){
		super();
		nectar = -400;
		view = new ImageView(new Image("File:deathPlant.png"));
		view.setX(xCord);
		view.setY(yCord);
	}

	public void finalize() throws Throwable {
		super.finalize();
	}

    /**
     * Gives death nectar
     * @return value of death nectar;
     */
	public int giveNectar(){
		//System.out.println("death nectar has been given");
		return nectar;
	}

}