package martensdr;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;

public class Deathwish extends BeeDecorator {
    private ImageView view = new ImageView(new Image("File:deathwishBee.png"));
    boolean removeDec = false;
    private ArrayList<Flower> flowers;

    public Deathwish(Bee deathWish) {
        bee = deathWish;
    }

    public void goToFlower(Flower flower) {
        if(flower.giveNectar() < 0){
            moveTick(flower);
        } else {

        }
    }
    public void moveTick(Flower flower){
        double xDif = xCord - flower.getxCord();
        double yDif = yCord - flower.getyCord();

        if (xDif < 0){
            xCord = xCord + 3;
        }
        else if ( xDif > 0){
            xCord = xCord -3;
        }

        if (yDif < 0){
            yCord = yCord +3;
        }
        if (yDif > 0){
            yCord = yCord - 3;
        }

        energy = energy -1;
    }
    /**
     * moves the bee left or right depending on the lap status
     */


    public ImageView getView() {
        return view;
    }

}
