package martensdr;

/*
 * SE 2811
 * Winter 2017/18
 * Lab 3
 * Names: Brandon Kandarapally, Danny Martens
 * Created: 12/28/2017
 */

import javafx.scene.image.ImageView;

import java.util.ArrayList;

/**
 * @author Brandon Kandarapally, Danny Martens
 * @version 1.0
 * @created 14-Dec-2017 3:52:09 PM
 * Abstract class for a Bee
 */

public abstract class Bee {

    /**
     * Variables used within Bee Class
     */
	protected int energy;
	protected double xCord;
	protected double yCord;
	protected ImageView view;

    /**
     * Constructor of required variables of any type of bee
     */
	public Bee(){
		energy = (int)(Math.random() * 600);
		xCord = (int)(Math.random() * 600);
		yCord = (int)(Math.random() *  600);
	}

    /**
     * Calculates distance between bee and a flower
     * @param flowerX X coordinate of flower
     * @param flowerY Y coordinate of flower
     * @return distance between bee and flower
     */
	public double calculateDistance(double flowerX, double flowerY){
		double x = xCord - flowerX;
		x = x*x;
		double y = yCord - flowerY;
		y = y*y;
		double total = x +y;
		return Math.sqrt(total);
	}

    /**
     * Detects collision between bee and and another bee
     * @param beeX Other bee's X coordinate
     * @param beeY Other bee's Y coordinate
     * @return
     */
    public double collision(double beeX, double beeY){
        double x = xCord - beeX;
        x = x*x;
        double y = yCord - beeY;
        y = y*y;

        double total = x + y;
        return Math.sqrt(total);
    }
	/**
	 *
	 * @param
     */
	public void checkForFlower(ArrayList<Flower> flowers,ArrayList<Bee> bees){

	}

    /**
     * Goes to flower and gets nectar from flower
     * @param flower a flower in the garden
     */
	public void goToFlower(Flower flower){
		energy = energy + flower.giveNectar();
		energy = energy - 1;
	}

    /**
     * How a bee will move,varies by bee type
     */
	public void moveTick(){

	}
	/**
	 * Determines if bee is dead if it's energy is less than or equal to 0
	 */
	public boolean isDead(){
		return energy<=0;
	}

    public ImageView getView() {
        return view;
    }

}